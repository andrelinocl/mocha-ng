import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from "@angular/router";


import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { HomeComponent } from "./home/home.component";
import { ProdutosComponent } from './produtos/produtos.component';
import { ProdutoComponent } from './produto/produto.component';
import { CartComponent } from "./cart/cart.component";

export const router: Routes = [
    {path: '', redirectTo: 'home', pathMatch: 'full'},
    {path: 'home', component: HomeComponent},
    {path: 'produtos', component: ProdutosComponent},
    {path: 'produto/:id', component: ProdutoComponent},
    {path: 'carrinho', component: CartComponent}
];

export const routes: ModuleWithProviders = RouterModule.forRoot(router);
