import { MochaNgPage } from './app.po';

describe('mocha-ng App', () => {
  let page: MochaNgPage;

  beforeEach(() => {
    page = new MochaNgPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
